OUTDIR ?= ~/.local/bin/

prep:
	nimble install nimterop
	nimble install illwill
	nimble install yaml
	nimble install daemon
	nimble install mpv

compile:
	cd src && nim c -d:release ricevilo.nim

install:
	mv src/ricevilo $(OUTDIR)
