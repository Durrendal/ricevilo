import std/[os, strutils, parseopt], daemon
import util, db, server

const doc = """
Usage:
  ricevilo             start TUI and media server
  ricevilo -h          helpful help that's helpful
  ricevilo -d          start media server daemon
  ricevilo -p          toggle play/pause
  ricevilo -r          refresh all shows
  ricevilo -q status   retrieve status of media server
  ricevilo -q episode  retrieve currently queued episode
  ricevilo -q show     retrieve show of currently queued episode
"""

const querydoc = """
Usage:

-q must be followed by a query filter

  ricevilo -q status   retrieve status of media server
  ricevilo -q episode  retrieve currently queued episode
  ricevilo -q show     retrieve show of currently queued episode
"""

proc main(): void =
  configure()
  
  var
    args = commandLineParams()

  if len(args) >= 1:
    case args[0]
    of "-h", "--help", "help":
      echo doc
    of "-q":
      if len(args) >= 2:
        case args[1]:
          of "show":
            let
              state = getState()
            echo getShowFromTitle(state.track)
          of "episode":
            let
              state = getState()
            echo state.track
          of "status":
            let
              state = getState()
            echo state.status
          else:
            echo querydoc
      else:
            echo querydoc
    of "-p":
      let
        state = getState().status
      echo "Current state is: " & state
      if state == "PLAY":
        ctrl("pause")
      elif state == "PAUSE" or state == "STOP":
        ctrl("play")
    of "-r":
      echo "refresh"
    of "-l":
      ctrl("quit")
    of "-d":
      #let
      #  pid = daemonize()
      #if pid == 0:
        dbInit(Conf.Database)
        seedDB()
        ctrlSrv()
  else:
    configureTheme()
    echo Conf
    echo Thematic
    
when isMainModule:
  main()

