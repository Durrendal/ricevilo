import std/[net, logging], mpv
import util, db
#Server functions, this provides a detachable media playback server.

#https://github.com/mpv-player/mpv-examples/blob/master/libmpv/wxwidgets/main.cpp
#https://github.com/mpv-player/mpv-examples/blob/master/libmpv/sdl/main.c
proc mpvCtx*(): ptr handle =
  let
    ctx = mpv.create()
  return ctx

proc configureMpv*(ctx: ptr handle) =
    ctx.set_option("terminal", "no")
    ctx.set_option("input-default-bindings", "no")
    ctx.set_option("input-vo-keyboard", "no")
    ctx.set_option("osc", true)
    check_error ctx.initialize()
    setState("STOP", "", 0)

proc queryMpv*(ctx: ptr handle, query: string): string =
  case query
  of "show":
    let
      episode = ctx.get_property("media-title")
    return getShowFromTitle(episode)
  of "episode":
    return ctx.get_property("media-title")
  of "status":
    let
      state = getServerStatus()
    return state

proc mpvctrl*(ctx: ptr handle, path: string) =
  ctx.command("loadfile", path)
  #ctx.command("seek", "300", "absolute", path)
  while true:
    let event = ctx.wait_event(10000)
    echo "event: ", mpv.event_name(event.event_id)
    if event.event_id == mpv.EVENT_SHUTDOWN:
      setState("STOP", "", 0)
    elif event.event_id == mpv.EVENT_END_FILE:
      setState("STOP", "", 0)
    elif event.event_id == mpv.EVENT_IDLE:
      setState("STOP", "", 0)
    elif event.event_id == mpv.EVENT_FILE_LOADED:
      setState("PLAY", queryMpv(ctx, "episode"), 0)
    elif event.event_id != mpv.EVENT_AUDIO_RECONFIG:
      echo queryMpv(ctx, "show")
      echo queryMpv(ctx, "episode")
      echo queryMpv(ctx, "status")

proc mpvTogglePause*(ctx: ptr handle) =
  ctx.set_option("pause", "yes")

proc mpvserver*(ctx: ptr handle) =
  configure()
  configurePods()
  configureMpv(ctx)
  mpvctrl(ctx, "test.opus")

#https://mpv.io/manual/stable/#properties
#https://mpv.io/manual/stable/#list-of-events

proc logit(str: string) =
  let f = open("err.log")
  defer: f.close()
  f.writeLine(str)

proc ctrlSrv*() =
  let
    server: Socket = newSocket()
    ctx = mpvCtx()
  server.bindAddr(Port(10455))
  server.listen()
  var client: Socket = new(Socket)
  server.accept(client)

  while true:
    let
      ctxcmd: string = client.recvLine()
    echo(ctxcmd)
    if ctxcmd == "quit":
      echo("got quit")
      quit(0)
    elif ctxcmd == "play":
      echo("got play")
      mpvserver(ctx)
    elif ctxcmd == "stop":
      echo("got stop")
      echo "stop"
    elif ctxcmd == "pause":
      echo("got pause")
      mpvTogglePause(ctx)
    elif ctxcmd == "resume":
      echo("got resume")
      echo "resume"
  server.close()

proc ctrl*(cmd: string) =
  let
    client: Socket = newSocket()
  client.connect("127.0.0.1", Port(10455))
  echo cmd
  client.send(cmd & "\r\L")
  client.close()
