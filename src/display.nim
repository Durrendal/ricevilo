import std/[os, strutils, sequtils, wordwrap], illwill
#TUI functions

proc exitProc() {.noconv.} =
  illwillDeinit()
  showCursor()
  quit(0)

#proc drawBuffer(): =

proc main(): void =
  illwillInit(fullscreen=true)
  setControlCHook(exitProc)
  hideCursor()

  drawBuff()

  while true:
    var
      key = getKey()
    case key
    of Key.None:
      discard
    of Key.Escape, Key.Q:
      exitProc()
    else:
      discard
      
    sleep(20)

main()
