import std/[os, db_sqlite, strutils], FeedNim
import util
#Database definitions, RSS Parsing, & Interaction

type Show* = tuple
    id: int
    title: string
    desc: string
    url: string

type NewShow* = tuple
    title: string
    desc: string
    url: string

type Episode* = tuple
    show: int
    title: string
    pubdate: string
    description: string
    length: string
    url: string

type State* = tuple
  id: int
  status: string
  track: string
  position: int

const state = sql"""
CREATE TABLE IF NOT EXISTS state (
id INTEGER PRIMARY KEY,
status STRING NOT NULL,
track STRING,
position INTEGER);
"""

const playback = sql"""
CREATE TABLE IF NOT EXISTS playback (
id INTEGER PRIMARY KEY,
FOREIGN KEY (episode) REFERENCES episodes (title) ON DELETE CASCADE,
FOREIGN KEY (show) REFERENCES podcasts (show) ON DELETE CASCADE,
length INTEGER,
position INTEGER,
);
"""

const version = sql"""
CREATE TABLE IF NOT EXISTS version (
db_ver INTEGER NOT NULL);
"""

const podcasts = sql"""
CREATE TABLE IF NOT EXISTS podcasts (
id INTEGER PRIMARY KEY,
show TEXT NOT NULL,
desc TEXT,
url TEXT NOT NULL,
created_on DATETIME DEFAULT CURRENT_TIMESTAMP);
"""

const episodes = sql"""
CREATE TABLE IF NOT EXISTS episodes (
id INTEGER PRIMARY KEY,
show INTEGER NOT NULL,
title TEXT NOT NULL,
pubdate TEXT NOT NULL,
desc TEXT,
length INTEGER NOT NULL, 
url TEXT NOT NULL,
created_on DATETIME DEFAULT CURRENT_TIMESTAMP,
FOREIGN KEY (show) REFERENCES podcasts (id) ON DELETE CASCADE);
"""

proc dbInit*(database: string) =
  let
    confdir = getEnv("HOME") & "/.config/ricevilo/"
    db = open(confdir & database, "", "", "")
  defer: db.close()
  db.exec(version)
  db.exec(state)
  db.exec(podcasts)
  db.exec(episodes)

proc getState*(): State =
  let
    confdir = getEnv("HOME") & "/.config/ricevilo/"
    db = open(confdir & Conf.Database, "", "", "")
  defer: db.close()
  for row  in db.rows(sql"SELECT * FROM state"):
    let
      instance: State = (id: parseInt(row[0]),
                         status: row[1],
                         track: row[2],
                         position: parseInt(row[3]))
    return instance

proc seedDB*() =
  let
    state = getState()
  if state.status == "":
    let
      confdir = getEnv("HOME") & "/.config/ricevilo/"
      db = open(confdir & Conf.Database, "", "", "")
    defer: db.close()
    db.exec(sql"INSERT INTO state (status, track, position) VALUES ('STOP', '', 0)")
    
proc getServerStatus*(): string =
  let
    state = getState()
  return state.status
  
proc getShows*(): seq[Show] =
  var
    shows = newSeq[Show]()
  let
    confdir = getEnv("HOME") & "/.config/ricevilo/"
    db = open(confdir & Conf.Database, "", "", "")
  defer: db.close()
  for row in db.rows(sql"SELECT * FROM podcasts"):
    let
      instance: Show = (id: parseInt(row[0]),
                        title: row[1],
                        desc: row[2],
                        url: row[3])
    shows.add(instance)
  return shows

proc setState*(status: string, track: string, position: int) =
  let
    confdir = getEnv("HOME") & "/.config/ricevilo/"
    db = open(confdir & Conf.Database, "", "", "")
  defer: db.close()
  db.exec(sql"UPDATE state SET status = ?, track = ?, position = ? WHERE id = 1", status, track, position)

proc getShow*(title: string): int =
  let
    confdir = getEnv("HOME") & "/.config/ricevilo/"
    db = open(confdir & Conf.Database, "", "", "")
  defer: db.close()
  let
    ret = db.getValue(sql"SELECT id FROM podcasts WHERE show = ?", title)
  if ret == "":
    return 0
  else:
    return parseInt(ret)

proc getEpisodes*(show: string): seq[Episode] =
  var
    episodes = newSeq[Episode]()
  let
    confdir = getEnv("HOME") & "/.config/ricevilo/"
    db = open(confdir & Conf.Database, "", "", "")
  defer: db.close()
  for row in db.rows(sql"SELECT * FROM podcasts WHERE show = ?", show):
    let
      instance: Episode = (show: parseInt(row[0]),
                           title: row[1],
                           pubdate: row[2],
                           description: row[3],
                           length: row[4],
                           url: row[5])
    episodes.add(instance)
  return episodes

proc getEpisode*(title: string, show: string): Episode =
  let
    confdir = getEnv("HOME") & "/.config/ricevilo/"
    db = open(confdir & Conf.Database, "", "", "")
  defer: db.close()
  for row in db.rows(sql"SELECT * FROM episodes WHERE title = ? AND show = ?", title, show):
    let
      instance: Episode = (show: parseInt(row[0]),
                           title: row[1],
                           pubdate: row[2],
                           description: row[3],
                           length: row[4],
                           url: row[5])
    return instance

proc getShowFromTitle*(title: string): string =
  let
    confdir = getEnv("HOME") & "/.config/ricevilo/"
    db = open(confdir & Conf.Database, "", "", "")
  defer: db.close()
  let
    show = db.getValue(sql"SELECT show FROM episodes WHERE title = ?", title)
    name = db.getValue(sql"SELECT show FROM podcasts WHERE id = ?", show)
  return name

proc getShowFromFeed*(url: string): NewShow =
  let
    feed = getRSS(url)
    instance: NewShow = (title: feed.title,
                         desc: feed.description,
                         url: feed.link)
  return instance

proc getShowFromAtom*(url: string): NewShow =
  let
    feed = getAtom(url)
  echo feed.title.text
  let
    instance: NewShow = (title: feed.title.text,
                         desc: feed.subtitle.text,
                         url: feed.link.href)
  return instance

proc getEpisodesFromFeed*(url: string): seq[Episode] =
  var
    episodes = newSeq[Episode]()
  let
    feed = getRSS(url)
    show = getShow(feed.title)

  for item in feed.items:
    let
      instance: Episode = (show: show,
                           title: item.title,
                           pubdate: item.pubDate,
                           description: item.description,
                           length: item.enclosure.length,
                           url: item.enclosure.url)
    episodes.add(instance)
  return episodes

proc getEpisodesFromAtom*(url: string): seq[Episode] =
  var
    episodes = newSeq[Episode]()
  let
    feed = getAtom(url)
    show = getShow(feed.title)

  for item in feed.entries:
    let
      instance: Episode = (show: show,
                           title: item.title.text,
                           pubdate: item.published,
                           description: item.summary,
                           length: item.link.href,
                           url: intToStr(item.link.length))
    episodes.add(instance)
  return episodes

proc addShow*(url: string, ftype: string) =
  let
    confdir = getEnv("HOME") & "/.config/ricevilo/"
    db = open(confdir & Conf.Database, "", "", "")
  defer: db.close()
  case ftype:
    of "rss":
      let
        show = getShowFromFeed(url)
      if db.getValue(sql"SELECT id FROM podcasts WHERE show = ? AND url = ?", show.title, show.url) == "":
        db.exec(sql"INSERT INTO podcasts (show, desc, url) VALUES (?, ?, ?)", show.title, show.desc, show.url)
    of "atom":
      let
        show = getShowFromAtom(url)
      if db.getValue(sql"SELECT id FROM podcasts WHERE show = ? AND url = ?", show.title, show.url) == "":
        db.exec(sql"INSERT INTO podcasts (show, desc, url) VALUES (?, ?, ?)", show.title, show.desc, show.url)
            
proc addEpisode*(episode: Episode) =
  let
    confdir = getEnv("HOME") & "/.config/ricevilo/"
    db = open(confdir & Conf.Database, "", "", "")
  defer: db.close()
  let
    exists = db.getValue(sql"SELECT id FROM episodes WHERE title = ? AND show = ?", episode.title, episode.show)
  if exists == "":
    db.exec(sql"INSERT INTO episodes (show, title, pubdate, desc, length, url) VALUES (?, ?, ?, ?, ?, ?)", episode.show, episode.title, episode.pubdate, episode.description, episode.length, episode.url)

proc addEpisodes*(url: string, ftype: string) =
  case ftype:
    of "rss":
      let
        episodes = getEpisodesFromFeed(url)
      echo episodes
      for episode in episodes:
        addEpisode(episode)
    of "atom":
      let
        episodes = getEpisodesFromAtom(url)
      echo episodes
      for episode in episodes:
        addEpisode(episode)

#configure()
#configurePods()
#echo Pods
#dbInit(Conf.Database)
#seedDB()
#for show in Pods:
#  addShow(show.Url, show.Type)
#  addEpisodes(show.Url, show.Type)
