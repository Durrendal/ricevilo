import std/[os, streams], yaml

type Feed* = tuple
  Url: string
  Type: string

type Podcasts* = seq[Feed]

type Config* = object
    Cache*: string
    Database*: string
    Theme*: string
    Podcasts*: string

type Element* = tuple
  Name: string
  Color: string

type Theme* = object
    Elements*: seq[Element]

var Conf*: Config
var Thematic*: Theme
var Pods*: Podcasts

proc configure*() =
  let
    confdir = getEnv("HOME") & "/.config/ricevilo/"
  var
    conf = confdir & "config.yaml"
    f = newFileStream(conf)
  load(f, Conf)
  f.close()

proc configurePods*() =
  let
    confdir = getEnv("HOME") & "/.config/ricevilo/"
  var
    conf = confdir & Conf.Podcasts
    f = newFileStream(conf)
  load(f, Pods)
  f.close()

proc configureTheme*() =
  let
    confdir = getEnv("HOME") & "/.config/ricevilo/"
  var
    conf = confdir & Conf.Theme
    f = newFileStream(conf)
  load(f, Thematic)
  f.close()
