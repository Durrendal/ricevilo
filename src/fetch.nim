import std/[os, httpclient, streams]

proc fetchFile*(url: string, outf: string) =
  var
    client = newHttpClient()

  try:
    var file = open(outf, fmwrite)
    defer: file.close()
    file.write(client.getContent(url))
  except IOError as err:
    echo("Failed to download from " & url & ": " & err.msg)

proc fetchFeed*(url: string): string =
  var
    client = newHttpClient()
  try:
    let
      feed = client.getContent(url)
    return feed
  except IOError as err:
    echo("Failed to fetch feed " & url & ": " & err.msg)
