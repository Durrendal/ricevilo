# What?

A TUI podcatcher in Nim!

# Installation

Build Depedencies:

```
mpv
mpv-dev
clang
clang-dev

```

Build:

```
make nimble
make build
make install
```

If you're on Alpine there's currently an issue with the nim package. It's missing the compiler sources. The best way to remedy this is to compile nim and pull those files in manually. I'll make an MR to fix that shortly. Assuming you clone the aports repo that looks like this:

```
cd aports/community/nim
abuild -r -K
cd src/nim-1.6.8/
cp -r compiler /usr/lib/nim/
```

Then install nimterop
```
nimble install nimterop
```

If you've installed nimterop previously and failed (like I did) you may end up with nimterop-#head in your ~/.nimble/pkgs. This will break library imports, to fix it:
```
cd ~/.nimble/pkgs
rm -rf nimterop-#head
```

Then try and compile again, so long as you've got nimterop installed successfully it should work.